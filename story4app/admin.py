from django.contrib import admin

# Register your models here.

from .models import PostForm

admin.site.register(PostForm)