from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'story4app'

urlpatterns = [
	url(r'^deleteurl/(?P<delete_id>\d+)/$', views.delete, name='delete'),
    path('', views.story_4, name='home'),
    path('characters/', views.characters, name='characters'),
    path('experiences/', views.experiences, name='experiences'),
	path('isian/', views.isian, name='isian'),
	path('hasilform/', views.hasilform, name='hasilform'),
]