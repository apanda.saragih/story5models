# Generated by Django 2.2.6 on 2019-10-05 18:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('story4app', '0003_auto_20191006_0108'),
    ]

    operations = [
        migrations.RenameField(
            model_name='post',
            old_name='alamat',
            new_name='email',
        ),
    ]
