# Generated by Django 2.2.6 on 2019-10-06 04:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story4app', '0004_auto_20191006_0108'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostForm',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('namaKegiatan', models.CharField(max_length=20)),
                ('tempat', models.TextField()),
                ('tanggal', models.TextField()),
                ('jam', models.TextField()),
                ('kategori', models.TextField()),
                ('published', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
