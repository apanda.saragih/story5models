from django.db import models

# Create your models here.
		
class PostForm(models.Model):
	namaKegiatan = models.CharField(max_length=20)
	tempat = models.TextField()
	tanggal = models.DateField()
	jam = models.TextField()
	kategori = models.TextField()
	published = models.DateTimeField(auto_now_add = True)
	def __str__(self):
		return "{}. {}".format(self.id, self.namaKegiatan)
		