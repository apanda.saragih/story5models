from django.shortcuts import render, redirect
from django import forms
from .forms import JadwalForm
from .models import PostForm
from django.http import HttpResponseRedirect

# Create your views here.
def story_4(request):
    return render(request, 'Story3.html')

def characters(request):
    return render(request, 'Characters.html')
	
def experiences(request):
	return render(request, 'Experiences.html')
	
def hasilform(request):
	postingan = PostForm.objects.all().order_by('tanggal')
	context = {
		'post': postingan,
	}
	return render(request, 'hasilform.html', context)
	
def isian(request):
	contact_form = JadwalForm(request.POST or None)
	error = None
	if contact_form.is_valid():
		if request.method == 'POST':
			PostForm.objects.create(
				namaKegiatan= contact_form.cleaned_data.get('nama_Kegiatan'),
				tempat		= contact_form.cleaned_data.get('tempat'),
				tanggal		= contact_form.cleaned_data.get('tanggal'),
				jam			= contact_form.cleaned_data.get('jam'),
				kategori	= contact_form.cleaned_data.get('kategori'),
			)
			return redirect('story4app:hasilform')
		else:
			error = contact_form.errors
		
	context = {
		'formm':contact_form,
	}
	return render(request, 'isian.html', context)

def delete(request, delete_id):
	PostForm.objects.filter(id=delete_id).delete()
	return redirect('story4app:hasilform')
