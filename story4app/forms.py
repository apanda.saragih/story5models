from django.shortcuts import render
from .models import PostForm
from django.forms.widgets import Widget, Select, MultiWidget
from django import forms

class JadwalForm(forms.Form):
	nama_Kegiatan = forms.CharField(
		widget = forms.TextInput(
				attrs={
					'class':'form-control',
					'placeholder':'Masukkan Nama Kegiatan Anda'
				}
		)
	)
	
	tempat = forms.CharField(
			widget = forms.TextInput(
				attrs={
					'class':'form-control',
					'placeholder':'Masukkan Tempat Kegiatan Anda'
				}
		)
	)

	tanggal = forms.DateField(
			widget=forms.SelectDateWidget(
					attrs={
						'class':'form-control col-sm-2',
					},
			)
	)
	
	jam = forms.TimeField(
		widget = forms.TextInput(
				attrs={
					'class':'form-control',
					'placeholder':'Masukkan Waktu Kegiatan Anda (Contoh : 04:05,  13:2'
				}
		)
	)
	
	kategori = forms.CharField(
		widget = forms.TextInput(
				attrs={
					'class':'form-control',
					'placeholder':'Masukkan Kategori Kegiatan Anda'
				}
		)
	)
	
	def clean_nama_Kegiatan(self):
		name = self.cleaned_data.get('nama_Kegiatan')
		if name == "ucup":
			raise forms.ValidationError("Input Salah")
		return name